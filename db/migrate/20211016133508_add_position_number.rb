# frozen_string_literal: true

class AddPositionNumber < ActiveRecord::Migration[6.1]
  def change
    add_column :sections, :position, :integer
    add_column :subsections, :position, :integer
    add_column :steps, :position, :integer
  end
end
