# frozen_string_literal: true

Rails.application.routes.draw do
  resources :sections, only: %i[create destroy index update], param: :id
  resource :sessions, only: %i[create destroy]
  resources :steps, only: %i[create destroy index update], param: :id
  resources :subsections, only: %i[create destroy index update], param: :id

  put "subsections/reorder/:id/:oldIndex/:newIndex", to: "subsections#reorder"

  root "home#index"
  get "*path", to: "home#index", via: :all
end
