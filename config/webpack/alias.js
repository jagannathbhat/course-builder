module.exports = {
  resolve: {
    alias: {
      actions: "src/actions",
      apis: "src/apis",
      common: "src/common",
      components: "src/components",
      constants: "src/constants",
      helpers: "src/helpers"
    }
  }
};
