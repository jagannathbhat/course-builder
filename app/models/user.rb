# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password
  has_secure_token :authentication_token

  validates :username, presence: true
  validates :password, length: { minimum: 6 }, if: -> { password.present? }
  validates :password_confirmation, presence: true, on: :create
end
