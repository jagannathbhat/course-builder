import React, { useState } from "react";

import { setStep } from "actions";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import stepsApi from "apis/steps";
import DeleteButton from "components/Common/DeleteButton";
import InputWithButton from "components/InputWithButton";

import InputAdd from "./InputAdd";

const AddStep = ({ setStep, step = { content: "" }, subsectionId }) => {
  const [content, setContent] = useState(step.content);
  const [loading, setLoading] = useState(false);

  const { id } = step;

  async function stepAdd(column) {
    setLoading(true);
    try {
      const response = await stepsApi.create({
        step: { column, content, subsection_id: subsectionId }
      });
      setStep("add", [response.data.step]);
    } catch (error) {
      logger.error(error);
    }
    setLoading(false);
  }

  async function stepDelete() {
    try {
      await stepsApi.destroy(id);
      setStep("delete", id);
    } catch (error) {
      logger.error(error);
    }
  }

  async function stepUpdate() {
    setLoading(true);
    try {
      const response = await stepsApi.update({
        id,
        payload: { step: { ...step, content } }
      });
      setStep("update", response.data.step);
    } catch (error) {
      logger.error(error);
    }
    setLoading(false);
  }

  return (
    <div className="flex-auto">
      {id ? (
        <div className="flex">
          <div className="flex-auto">
            <InputWithButton
              disabled={loading}
              iconClass="ri-check-line"
              value={content}
              onChange={e => setContent(e.target.value)}
              onSubmit={() => stepUpdate()}
            />
          </div>
          <DeleteButton onClick={stepDelete} />
        </div>
      ) : (
        <InputAdd loading={loading} stepAdd={stepAdd} setContent={setContent} />
      )}
    </div>
  );
};

AddStep.propTypes = {
  step: PropTypes.object,
  subsectionId: PropTypes.number
};

export default connect(null, { setStep })(AddStep);
