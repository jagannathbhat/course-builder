import React from "react";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import AddStep from "./AddStep";

const Steps = ({ editOutline, steps, subsectionId }) => (
  <>
    {steps
      .filter(step => step.subsection_id === subsectionId)
      .map((step, index) => (
        <div
          key={index}
          className={
            "flex items-start mt-4" +
            (step.column === 1 ? "" : " flex-row-reverse")
          }
        >
          {editOutline ? (
            <AddStep subsectionId={subsectionId} step={step} />
          ) : (
            <div className="flex-1 text-gray-700">{step.content}</div>
          )}
          <div className="flex-1"></div>
        </div>
      ))}
    {editOutline && (
      <div className="mt-4 py-2">
        <AddStep subsectionId={subsectionId} />
      </div>
    )}
  </>
);

Steps.propTypes = {
  editOutline: PropTypes.bool,
  subsectionId: PropTypes.number
};

const mapStateToProps = ({ steps }) => ({ steps });

export default connect(mapStateToProps)(Steps);
