import React, { useRef, useState } from "react";

import { setSubsection } from "actions";
import { connect } from "react-redux";

import subsectionsApi from "apis/subsections";

import Script from "./Script";
import Subsections from "./Subsections";

const Storyboard = ({ setSubsection }) => {
  const timer = useRef(null);
  const [saveMessage, setSaveMessage] = useState("");
  const [scriptSubsection, setScriptSubsection] = useState(null);
  const [scriptText, setScriptText] = useState("");
  const [showScript, setShowScript] = useState(false);
  const [editOutline, setEditOutline] = useState(false);

  async function scriptClose() {
    try {
      setShowScript(false);
    } catch (error) {
      logger.error(error);
    }
  }

  async function scriptOpen(id, text) {
    setScriptSubsection(id);
    setScriptText(text);
    setShowScript(true);
  }

  async function scriptUpdate(script) {
    if (timer.current) clearTimeout(timer.current);

    setScriptText(script);
    setSaveMessage("Saving...");

    timer.current = setTimeout(async () => {
      try {
        const response = await subsectionsApi.update({
          id: scriptSubsection,
          payload: { subsection: { script } }
        });
        setSubsection("update", response.data.subsection);
        setSaveMessage("All changes saved.");
      } catch (error) {
        logger.error(error);
      }
    }, 2000);
  }

  return (
    <div className="max-w-2xl mx-auto">
      <div className="flex items-center justify-between my-4">
        <h2 className="text-center text-2xl">Storyboard</h2>
        <button
          className="text-center text-gray-500 text-sm focus:underline"
          onClick={() => setEditOutline(!editOutline)}
        >
          {editOutline ? "Done" : "Edit"}
        </button>
      </div>
      <Subsections editOutline={editOutline} scriptOpen={scriptOpen} />
      {showScript && (
        <Script
          closeScript={scriptClose}
          onChange={e => scriptUpdate(e.target.value)}
          saveMessage={saveMessage}
          scriptText={scriptText}
          setScriptText={setScriptText}
        />
      )}
    </div>
  );
};

export default connect(null, { setSubsection })(Storyboard);
