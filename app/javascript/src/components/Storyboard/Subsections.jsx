import React from "react";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import Steps from "./Steps";

const Subsections = ({ editOutline, scriptOpen, sections }) => (
  <>
    {sections.map(section =>
      section.subsections.map((subsection, index) => (
        <div key={index} className="bg-white my-4 p-4 shadow">
          <div className="flex justify-between items-center">
            <h3 className="text-xl">
              {subsection.name + " "}
              <small className="text-sm text-gray-600">{section.name}</small>
            </h3>
            <button
              onClick={() =>
                scriptOpen(
                  subsection.id,
                  subsection.script ? subsection.script : ""
                )
              }
            >
              <i className="ri-sticky-note-line"></i>
            </button>
          </div>
          <Steps editOutline={editOutline} subsectionId={subsection.id} />
        </div>
      ))
    )}
  </>
);

Subsections.propTypes = {
  editOutline: PropTypes.bool,
  scriptOpen: PropTypes.func
};

const mapStateToProps = ({ sections }) => ({ sections });

export default connect(mapStateToProps)(Subsections);
