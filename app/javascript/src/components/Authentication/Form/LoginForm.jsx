import React from "react";

import PropTypes from "prop-types";

import Button from "components/Button";
import Input from "components/Input";

const LoginForm = ({ handleSubmit, loading, setPassword, setUsername }) => {
  return (
    <div
      className="flex items-center justify-center min-h-screen
      px-4 py-12 lg:px-8 bg-gray-50 sm:px-6"
    >
      <div className="w-full max-w-md">
        <h2
          className="mt-6 text-3xl font-extrabold leading-9
          text-center text-bb-gray-700"
        >
          Course Builder
        </h2>
        <form className="mt-8" onSubmit={handleSubmit}>
          <Input
            autoFocus={true}
            label="Username"
            type="username"
            placeholder="username"
            onChange={e => setUsername(e.target.value)}
          />
          <Input
            label="Password"
            type="password"
            placeholder="********"
            onChange={e => setPassword(e.target.value)}
          />
          <Button type="submit" loading={loading}>
            Sign In
          </Button>
        </form>
      </div>
    </div>
  );
};

LoginForm.propTypes = {
  handleSubmit: PropTypes.func,
  loading: PropTypes.bool,
  setPassword: PropTypes.func,
  setUsername: PropTypes.func
};

export default LoginForm;
