import React, { useState } from "react";

import { setToLocalStorage } from "helpers/storage";

import authApi from "apis/auth";
import { setAuthHeaders } from "apis/axios";
import { fetchData } from "common/fetchData";
import LoginForm from "components/Authentication/Form/LoginForm";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  const handleSubmit = async event => {
    event.preventDefault();
    try {
      const response = await authApi.login({ login: { username, password } });
      setToLocalStorage({
        authToken: response.data.authentication_token,
        userId: response.data.id,
        username
      });
      setAuthHeaders();
      await fetchData(setLoading);
      window.location.href = "/";
    } catch (error) {
      logger.error(error);
      setLoading(false);
    }
  };

  return (
    <LoginForm
      setUsername={setUsername}
      setPassword={setPassword}
      loading={loading}
      handleSubmit={handleSubmit}
    />
  );
};

export default Login;
