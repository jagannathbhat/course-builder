import React, { useState } from "react";

import Sections from "./Sections";

const Outline = () => {
  const [editOutline, setEditOutline] = useState(false);

  return (
    <div className="max-w-2xl mx-auto mb-4">
      <div className="flex items-center justify-between my-4">
        <h2 className="text-center text-2xl">Outline</h2>
        <button
          className="text-center text-gray-500 text-sm focus:underline"
          onClick={() => setEditOutline(!editOutline)}
        >
          {editOutline ? "Done" : "Edit"}
        </button>
      </div>
      <Sections editOutline={editOutline} />
    </div>
  );
};

export default Outline;
