import React from "react";

import Edit from "./Edit";
import List from "./List";

const Sections = ({ editOutline }) => {
  return editOutline ? <Edit /> : <List />;
};

export default Sections;
