import React, { useEffect, useRef } from "react";

import { setSubsection } from "actions";
import { connect } from "react-redux";
import Sortable from "sortablejs";

import subsectionsApi from "apis/subsections";
import sortPositions from "common/sortPositions";

import AddSubsection from "./AddSubsection";
import Subsection from "./Subsection";

const Subsections = ({ sectionId, setSubsection, subsections }) => {
  const sortableList = useRef(null);
  const sortedSubsections = subsections.sort(sortPositions);

  async function reorderList(oldIndex, newIndex) {
    const newList = sortedSubsections;

    const elem = newList[oldIndex];

    if (oldIndex < newIndex) {
      for (let i = oldIndex; i < newIndex; i++)
        newList[i] = { ...newList[i + 1], position: i };
    } else {
      for (let i = oldIndex; i > newIndex; i--)
        newList[i] = { ...newList[i - 1], position: i };
    }

    newList[newIndex] = { ...elem, position: newIndex };

    setSubsection("reorder", {
      section: { id: sectionId },
      subsections: newList
    });

    try {
      await subsectionsApi.reorder({ id: sectionId, oldIndex, newIndex });
    } catch (error) {
      logger.error(error);
    }
  }

  async function deleteSubsection(id) {
    try {
      await subsectionsApi.destroy(id);
      setSubsection("delete", {
        section: { id: sectionId },
        subsection: { id }
      });
    } catch (error) {
      logger.error(error);
    }
  }

  useEffect(() => {
    if (sortableList.current) {
      const sortableConfig = {
        onEnd: e => {
          reorderList(e.oldIndex, e.newIndex);
        }
      };
      Sortable.create(sortableList.current, sortableConfig);
    }
  }, [sortableList.current]);

  return (
    <>
      {sortedSubsections.length > 0 && (
        <div className="space-y-8" ref={sortableList}>
          {sortedSubsections.map(({ id, name }, index) => (
            <Subsection
              key={index}
              deleteSubsection={deleteSubsection}
              id={id}
              name={name}
              sectionId={sectionId}
            />
          ))}
        </div>
      )}
      <div className="mt-4 py-2">
        <AddSubsection sectionId={sectionId} />
      </div>
    </>
  );
};

export default connect(null, { setSubsection })(Subsections);
