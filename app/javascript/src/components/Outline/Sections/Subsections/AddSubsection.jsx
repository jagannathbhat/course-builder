import React, { useState } from "react";

import { setSubsection } from "actions";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import subsectionsApi from "apis/subsections";
import InputWithButton from "components/InputWithButton";

const AddSubsection = ({ id, oldName = "", sectionId, setSubsection }) => {
  const [loading, setLoading] = useState(false);
  const [name, setName] = useState(oldName);

  async function subsectionAdd() {
    setLoading(true);
    try {
      const response = await subsectionsApi.create({
        subsection: { name, section_id: sectionId }
      });
      setSubsection("add", {
        section: { id: sectionId },
        subsection: response.data.subsection
      });
      setName("");
      setLoading(false);
    } catch (error) {
      logger.error(error);
      setLoading(false);
    }
  }

  async function subsectionUpdate() {
    setLoading(true);
    try {
      const response = await subsectionsApi.update({
        id,
        payload: { subsection: { name } }
      });
      setSubsection("update", {
        section: { id: sectionId },
        subsection: response.data.subsection
      });
      setLoading(false);
    } catch (error) {
      logger.error(error);
      setLoading(false);
    }
  }

  return (
    <div className="flex-auto">
      <InputWithButton
        disabled={loading}
        iconClass={id ? "ri-check-line" : "ri-add-line"}
        placeholder="Add Subsection"
        value={name}
        onChange={e => setName(e.target.value)}
        onSubmit={() => (id ? subsectionUpdate() : subsectionAdd())}
      />
    </div>
  );
};

AddSubsection.propTypes = {
  id: PropTypes.number,
  oldName: PropTypes.string,
  sectionId: PropTypes.number
};

export default connect(null, { setSubsection })(AddSubsection);
