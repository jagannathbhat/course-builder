import React from "react";

import DeleteButton from "components/Common/DeleteButton";
import MoveHandle from "components/Common/MoveHandle";

import AddSubsection from "./AddSubsection";

const Subsection = ({ deleteSubsection, id, name, sectionId }) => {
  return (
    <div className="flex items-center justify-between pl-6">
      <MoveHandle />
      <AddSubsection id={id} oldName={name} sectionId={sectionId} />
      <DeleteButton onClick={() => deleteSubsection(id)} />
    </div>
  );
};

export default Subsection;
