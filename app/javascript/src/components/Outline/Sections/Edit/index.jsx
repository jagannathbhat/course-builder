import React from "react";

import { setSection } from "actions";
import { connect } from "react-redux";

import sectionsApi from "apis/sections";

import AddSection from "./AddSection";
import EditSection from "./EditSection";

const Edit = ({ sections, setSection }) => {
  async function deleteSection(id) {
    try {
      await sectionsApi.destroy(id);
      setSection("delete", id);
    } catch (error) {
      logger.error(error);
    }
  }

  if (sections.length === 0) {
    return (
      <p className="text-center text-gray-500">
        Click on edit to add a new section
      </p>
    );
  }

  return (
    <div className="space-y-4">
      {sections.map(({ id, name, subsections }, index) => (
        <EditSection
          key={index}
          deleteSection={deleteSection}
          id={id}
          name={name}
          subsections={subsections}
        />
      ))}
      <AddSection />
    </div>
  );
};

const mapStateToProps = ({ sections }) => ({ sections });

export default connect(mapStateToProps, { setSection })(Edit);
