import React from "react";

import DeleteButton from "components/Common/DeleteButton";

import AddSection from "./AddSection";

import SectionCard from "../SectionCard";
import Subsections from "../Subsections";

const EditSection = ({ deleteSection, id, name, subsections }) => {
  return (
    <SectionCard>
      <div className="flex items-center justify-between">
        <AddSection id={id} oldName={name} />
        <DeleteButton onClick={() => deleteSection(id)} />
      </div>
      <Subsections sectionId={id} subsections={subsections} />
    </SectionCard>
  );
};

export default EditSection;
