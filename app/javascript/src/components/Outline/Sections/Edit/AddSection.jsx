import React, { useState } from "react";

import { setSection } from "actions";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import sectionsApi from "apis/sections";
import InputWithButton from "components/InputWithButton";

const AddSection = ({ id, oldName = "", setSection }) => {
  const [loading, setLoading] = useState(false);
  const [name, setName] = useState(oldName);

  async function sectionAdd() {
    setLoading(true);
    try {
      const response = await sectionsApi.create({ section: { name } });
      setSection("add", [response.data.section]);
      setName("");
      setLoading(false);
    } catch (error) {
      logger.error(error);
      setLoading(false);
    }
  }

  async function sectionUpdate() {
    setLoading(true);
    try {
      const response = await sectionsApi.update({
        id,
        payload: { section: { name } }
      });
      setSection("update", response.data.section);
      setLoading(false);
    } catch (error) {
      logger.error(error);
      setLoading(false);
    }
  }

  return (
    <div className="flex-auto">
      <InputWithButton
        disabled={loading}
        iconClass={id ? "ri-check-line" : "ri-add-line"}
        placeholder="Add Section"
        value={name}
        onChange={e => setName(e.target.value)}
        onSubmit={() => (id ? sectionUpdate() : sectionAdd())}
      />
    </div>
  );
};

AddSection.propTypes = {
  id: PropTypes.number,
  oldName: PropTypes.string
};

export default connect(null, { setSection })(AddSection);
