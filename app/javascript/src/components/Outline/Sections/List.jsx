import React from "react";

import { setSection } from "actions";
import { connect } from "react-redux";

import sortPositions from "common/sortPositions";

import SectionCard from "./SectionCard";

const List = ({ sections }) => {
  return (
    <div className="space-y-4">
      {sections.map((section, sectionNo) => (
        <SectionCard key={sectionNo}>
          <h3>{`${sectionNo + 1}. ${section.name}`}</h3>
          {section.subsections
            .sort(sortPositions)
            .map((subsection, subsectionNo) => (
              <h4 key={subsectionNo} className="pl-8">{`${sectionNo + 1}.${
                subsectionNo + 1
              }. ${subsection.name}`}</h4>
            ))}
        </SectionCard>
      ))}
    </div>
  );
};

const mapStateToProps = ({ sections }) => ({ sections });

export default connect(mapStateToProps, { setSection })(List);
