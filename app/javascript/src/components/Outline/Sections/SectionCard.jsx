import React from "react";

const SectionCard = ({ children }) => (
  <div className="bg-white p-4 pb-8 shadow space-y-8">{children}</div>
);

export default SectionCard;
