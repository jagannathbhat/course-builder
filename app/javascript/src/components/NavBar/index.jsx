import React from "react";

import { setToLocalStorage } from "helpers/storage.js";

import authApi from "apis/auth";
import { resetAuthTokens } from "apis/axios";

import NavItem from "./NavItem";

const NavBar = () => {
  async function handleLogout() {
    try {
      await authApi.logout();
      setToLocalStorage({
        authToken: null,
        userId: null,
        username: null
      });
      resetAuthTokens();
      window.location.href = "/";
    } catch (error) {
      logger.error(error);
    }
  }

  return (
    <nav className="bg-white shadow">
      <div className="mx-auto max-w-7xl px-2 sm:px-4 lg:px-8">
        <div className="flex h-16 items-center justify-between">
          <div className="px-2 lg:px-0">
            <h1 className="text-lg">Course Builder</h1>
          </div>
          <div className="flex">
            <div className="px-2 lg:px-0">
              <NavItem name="Outline" path="/" />
            </div>
            <div className="px-2 lg:px-0">
              <NavItem name="Storyboard" path="/storyboard" />
            </div>
            <div className="px-2 lg:px-0">
              <NavItem name="Logout" onClick={handleLogout} />
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
