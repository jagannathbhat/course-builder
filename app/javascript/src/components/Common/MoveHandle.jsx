import React from "react";

const MoveHandle = () => (
  <div className="p-1">
    <i className="ri-drag-move-2-line"></i>
  </div>
);

export default MoveHandle;
