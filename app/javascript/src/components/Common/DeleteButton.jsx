import React from "react";

import PropTypes from "prop-types";

const DeleteButton = ({ onClick }) => (
  <button className="p-1 ml-2" onClick={onClick}>
    <i className="ri-delete-bin-7-line"></i>
  </button>
);

DeleteButton.propTypes = { onClick: PropTypes.func };

export default DeleteButton;
