export const setSection = (type, payload) => async dispatch => {
  try {
    dispatch({ type: type + "_section", payload });
  } catch (err) {
    logger.error(err);
  }
};

export const setStep = (type, payload) => async dispatch => {
  try {
    dispatch({ type: type + "_step", payload });
  } catch (err) {
    logger.error(err);
  }
};

export const setSubsection = (type, payload) => async dispatch => {
  try {
    dispatch({ type: type + "_subsection", payload });
  } catch (err) {
    logger.error(err);
  }
};
