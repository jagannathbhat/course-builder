import React, { useEffect, useState } from "react";

import { getFromLocalStorage } from "helpers/storage";
import { either, isEmpty, isNil } from "ramda";
import { Provider } from "react-redux";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import { ToastContainer } from "react-toastify";

import { registerIntercepts, setAuthHeaders } from "apis/axios";
import { fetchData } from "common/fetchData";
import { initializeLogger } from "common/logger";
import Login from "components/Authentication/Login";
import PrivateRoute from "components/Common/PrivateRoute";
import NavBar from "components/NavBar";
import Outline from "components/Outline";
import PageLoader from "components/PageLoader";
import Storyboard from "components/Storyboard";

import store from "./store";

const App = () => {
  const [loading, setLoading] = useState(true);

  const authToken = getFromLocalStorage("authToken");
  const isLoggedIn = !either(isNil, isEmpty)(authToken) && authToken !== "null";

  useEffect(() => {
    /*eslint no-undef: "off"*/
    initializeLogger();
    registerIntercepts();
    setAuthHeaders();

    if (isLoggedIn) fetchData(setLoading);
    else setLoading(false);
  }, []);

  if (loading) {
    return (
      <div className="w-screen h-screen">
        <PageLoader />
      </div>
    );
  }

  return (
    <Provider store={store}>
      <Router>
        {isLoggedIn && <NavBar />}
        <ToastContainer />
        <Switch>
          <Route exact path="/login" component={Login} />
          <PrivateRoute
            component={Outline}
            condition={isLoggedIn}
            exact
            path="/"
            redirectRoute="/login"
          />
          <PrivateRoute
            component={Storyboard}
            condition={isLoggedIn}
            exact
            path="/storyboard"
            redirectRoute="/login"
          />
        </Switch>
      </Router>
    </Provider>
  );
};

export default App;
