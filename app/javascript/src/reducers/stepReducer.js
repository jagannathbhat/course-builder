const initialState = [];

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case "add_step":
      return state.concat(payload);
    case "delete_step":
      return state.filter(step => step.id !== payload);
    case "update_step": {
      const index = state.findIndex(step => step.id === payload.id);
      const newState = [...state];
      newState[index] = payload;
      return newState;
    }
    default:
      return state;
  }
};
