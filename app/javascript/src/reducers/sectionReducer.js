const initialState = [];

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case "add_section":
      return state.concat(payload);
    case "delete_section":
      return state.filter(section => section.id !== payload);
    case "update_section": {
      const index = state.findIndex(section => section.id === payload.id);
      const newState = [...state];
      newState[index] = payload;
      return newState;
    }
    case "add_subsection":
      return state.map(section => {
        if (section.id === payload.section.id) {
          section.subsections = section.subsections.concat([
            payload.subsection
          ]);
        }
        return section;
      });
    case "delete_subsection":
      return state.map(section => {
        if (section.id === payload.section.id) {
          section.subsections = section.subsections.filter(
            subsection => subsection.id !== payload.subsection.id
          );
        }
        return section;
      });
    case "reorder_subsection": {
      return state.map(section => {
        if (section.id === payload.section.id) {
          section.subsections = payload.subsections;
        }
        return section;
      });
    }
    case "update_subsection":
      return state.map(section => {
        if (section.id === payload.section.id) {
          section.subsections = section.subsections.map(subsection =>
            subsection.id === payload.subsection.id
              ? payload.subsection
              : subsection
          );
        }
        return section;
      });
    default:
      return state;
  }
};
