import { combineReducers } from "redux";

import sectionReducer from "./sectionReducer.js";
import stepReducer from "./stepReducer.js";

export default combineReducers({
  sections: sectionReducer,
  steps: stepReducer
});
