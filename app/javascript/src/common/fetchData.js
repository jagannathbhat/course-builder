import { setSection, setStep } from "actions";

import sectionsApi from "apis/sections";
import stepsApi from "apis/steps";

import store from "../store";

export async function fetchData(setLoading) {
  try {
    let response = await sectionsApi.list();
    store.dispatch(setSection("add", response.data.sections));
    response = await stepsApi.list();
    store.dispatch(setStep("add", response.data.steps));
  } catch (error) {
    logger.error(error);
  }
  setLoading(false);
}
