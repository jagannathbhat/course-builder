export default (a, b) => {
  if (a.position > b.position) return 1;
  else if (a.position === b.position) return 0;
  else return -1;
};
