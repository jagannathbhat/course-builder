const setToLocalStorage = ({ authToken, userId, username }) => {
  localStorage.setItem("authToken", authToken);
  localStorage.setItem("authUserId", userId);
  localStorage.setItem("username", username);
};

const getFromLocalStorage = key => {
  return localStorage.getItem(key);
};

export { setToLocalStorage, getFromLocalStorage };
