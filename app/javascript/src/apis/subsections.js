import axios from "axios";

const create = payload => axios.post("/subsections/", payload);

const destroy = id => axios.delete(`/subsections/${id}`);

const list = () => axios.get("/subsections");

const reorder = ({ id, oldIndex, newIndex }) =>
  axios.put(`/subsections/reorder/${id}/${oldIndex}/${newIndex}`);

const update = ({ id, payload }) => axios.put(`/subsections/${id}`, payload);

const subsectionsApi = {
  create,
  destroy,
  list,
  reorder,
  update
};

export default subsectionsApi;
