# frozen_string_literal: true

class SubsectionsController < ApplicationController
  before_action :authenticate_user_using_x_auth_token
  before_action :load_subsection, only: %i[destroy update]

  def create
    @subsection = Subsection.new(subsection_params)
    @subsection.position = Subsection.where(section_id: @subsection.section_id).length
    if @subsection.save
      render status: :ok,
        json: { notice: t("successfuly_created", entity: "subsection"), subsection: @subsection }
    else
      errors = @subsection.errors.full_messages.to_sentence
      render status: :unprocessable_entity, json: { errors: errors }
    end
  end

  def destroy
    if @subsection.destroy
      render status: :ok, json: { notice: t("successfuly_deleted", entity: "subsection") }
    else
      errors = @subsection.errors.full_messages.to_sentence
      render status: :unprocessable_entity, json: { errors: errors }
    end
  end

  def index
    subsections = Subsection.all
    render status: :ok, json: { subsections: subsections }
  end

  def reorder
    begin
      @subsections = Subsection.where(section_id: params[:id])
      puts @subsections.as_json

      newIndex = params[:newIndex].to_i
      oldIndex = params[:oldIndex].to_i

      target = @subsections.find_by!(position: oldIndex.to_s)

      if oldIndex < newIndex
        for i in oldIndex + 1 .. newIndex do
          subsection = @subsections.find_by!(position: i.to_s)
          subsection.position = i - 1
          subsection.save!
        end
      else
        for i in newIndex ... oldIndex do
          subsection = @subsections.find_by!(position: i.to_s)
          subsection.position = i + 1
          subsection.save!
        end
      end

      target.position = newIndex
      target.save!

      render status: :ok, json: { notice: t("successfuly_reordered", entity: "subsections") }
    rescue
      render status: :internal_server_error, json: { notice: t("internal_server_error") }
    end
  end

  def update
    if @subsection.update(subsection_params)
      render status: :ok, json: { notice: t("successfuly_updated", entity: "subsection"), subsection: @subsection }
    else
      render status: :unprocessable_entity, json: { errors: @subsection.errors.full_messages.to_sentence }
    end
  end

  private

    def load_subsection
      @subsection = Subsection.find_by_id!(params[:id])
    rescue ActiveRecord::RecordNotFound => errors
      render json: { errors: errors }
    end

    def subsection_params
      params.require(:subsection).permit(:name, :position, :section_id, :script)
    end
end
