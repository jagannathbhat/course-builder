# frozen_string_literal: true

class ApplicationController < ActionController::Base
  def authenticate_user_using_x_auth_token
    auth_token = request.headers["X-Auth-Token"].presence
    username = request.headers["X-Auth-Username"]

    user = username && User.find_by_username(username)

    if user && auth_token &&
      ActiveSupport::SecurityUtils.secure_compare(
        user.authentication_token, auth_token
      )
      @current_user = user
    else
      render status: :unauthorized, json: { error: t("session.could_not_auth") }
    end
  end

  private

    def current_user
      @current_user
    end
end
