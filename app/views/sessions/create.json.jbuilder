# frozen_string_literal: true

json.extract! @user,
  :authentication_token,
  :id,
  :username
