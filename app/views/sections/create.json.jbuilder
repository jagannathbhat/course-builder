# frozen_string_literal: true

# json: { notice: t("successfuly_created", entity: "section"), section: @section }

json.notice t("successfuly_created", entity: "section")

json.section do
  json.extract! @section, :id, :name, :position
  json.subsections []
end
