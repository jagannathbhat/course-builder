# frozen_string_literal: true

json.sections do
  json.array! @sections do |section|
    json.extract! section, :id, :name, :position
    json.subsections do
      json.array! section.subsections do |subsection|
        json.extract! subsection, :id, :name, :position, :script
      end
    end
  end
end
